<?php get_header(); ?>

    <?php get_template_part('templates/archive-resources/hero'); ?>

    <?php get_template_part('templates/archive-resources/page-header'); ?>

    <section class="resources-container grid">

        <?php get_template_part('templates/archive-resources/sidebar'); ?>

        <?php get_template_part('templates/archive-resources/list'); ?>

    </section>

<?php get_footer(); ?>