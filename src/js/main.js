import Header from './header.js';
import Animations from './animations.js';
import Utilities from './utilities.js';
import Content from './content.js';
import Modals from './modals.js';

(() => {
    Header.init();
    Animations.init();
    Utilities.init();
    Modals.init();
    Content.init();
})();
