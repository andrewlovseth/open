<?php get_header(); ?>

    <section class="page-header grid">
        <h1 class="page-title">Customer Stories</h1>
    </section>

    <?php get_template_part('templates/archive-customer-stories/featured'); ?>

    <?php get_template_part('templates/archive-customer-stories/grid'); ?>

<?php get_footer(); ?>