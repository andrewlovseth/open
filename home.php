<?php get_header(); ?>
	<?php get_template_part('templates/company/nav'); ?>

    <?php get_template_part('templates/archive-post/blog-header'); ?>

    <?php get_template_part('templates/archive-post/featured'); ?>

    <?php get_template_part('templates/archive-post/archive-grid'); ?>

<?php get_footer(); ?>