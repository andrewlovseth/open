<?php

    $embed = get_field('listings_embed');

?>

<section class="listings grid">

    <div class="embed">
        <?php echo $embed; ?>
    </div>
    
</section>