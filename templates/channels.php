<?php

/*
	Template Name: Channels
*/

get_header(); ?>

	<?php get_template_part('template-parts/global/hero'); ?>
	
	<?php get_template_part('templates/channels/intro'); ?>

	<?php get_template_part('templates/channels/featured'); ?>

	<?php get_template_part('templates/channels/why'); ?>

	<?php get_template_part('templates/channels/partners'); ?>

	<?php get_template_part('templates/channels/form'); ?>

<?php get_footer(); ?>