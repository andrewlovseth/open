<?php

/*
	Template Name: API Docs
*/

get_header(); ?>

	<?php get_template_part('templates/api-docs/hero'); ?>
	
	<?php get_template_part('templates/api-docs/intro'); ?>
    
<?php get_footer(); ?>