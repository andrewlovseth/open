<?php

/*
	Template Name: Tech Alliances
*/

get_header(); ?>

	<?php get_template_part('template-parts/global/hero'); ?>

    <?php get_template_part('templates/tech-alliances/intro'); ?>

	<?php get_template_part('templates/tech-alliances/why'); ?>

	<?php get_template_part('templates/tech-alliances/partners'); ?>

	<?php get_template_part('templates/tech-alliances/form'); ?>

<?php get_footer(); ?>