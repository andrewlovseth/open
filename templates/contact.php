<?php

/*
	Template Name: Contact
*/

get_header(); ?>

	<?php get_template_part('templates/contact/hero'); ?>
	
	<?php get_template_part('templates/company/nav'); ?>

	<?php get_template_part('templates/contact/page-header'); ?>

	<?php get_template_part('templates/contact/contact-info'); ?>
    
<?php get_footer(); ?>