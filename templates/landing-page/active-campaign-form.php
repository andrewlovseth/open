<?php

    $embed = get_sub_field('embed');

?>

<section class="active-campaign-form grid">

    <div class="embed">
        <?php echo $embed; ?>
    </div>

</section>