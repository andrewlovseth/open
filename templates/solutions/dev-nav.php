<div class="dev-nav grid">
    <ul>
        <li><a href="/solutions/cloud-hybrid-remote/">Cloud, Hybrid & Remote</a></li>
        <li><a href="/solutions/digital-media/">Digital Media</a></li>
        <li><a href="/solutions/broadcast/">Broadcast</a></li>
        <li><a href="/solutions/video-surveillance/">Video Surveillance</a></li>
        <li><a href="/solutions/data-protection-and-backup/">Data Protection & Backup</a></li>
        <li><a href="/solutions/data-analytics/">Data Analytics</a></li>
        <li><a href="/solutions/containerization/">Containerization</a></li>
        <li><a href="/solutions/general-purpose-nas/">General Purpose NAS</a></li>
    </ul>
</div>