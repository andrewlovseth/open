<?php

/*
	Template Name: Careers
*/

get_header(); ?>

	<?php get_template_part('templates/careers/hero'); ?>

	<?php get_template_part('templates/company/nav'); ?>

	<?php get_template_part('templates/careers/page-header'); ?>

	<?php get_template_part('templates/careers/listings'); ?>
    
<?php get_footer(); ?>