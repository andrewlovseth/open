<article class="search-result no-results">
    <div class="info">
        <h3>No results found.</h3>

        <div class="copy copy-2">
            <p>Please try a different query.</p>
        </div>
    </div>
</article>