<?php

    $args = wp_parse_args($args);
    if(!empty($args)) {
        $item = $args['item']; 
    }

    $post_type = get_post_type( $item->ID );
    $search = get_field('search', $item->ID);
    $photo = $search['photo'];
    $description = $search['description'];


    if($search['title']) {
        $title = $search['title'];
    } else {
        $title = get_the_title( $item->ID );
    }

    if($search['label']) {
        $term = $search['label'];
    } else {
        $terms = get_the_terms( $item->ID, 'resource_type');
        $term = $terms[0]->name;
    }

?>

<div class="item resource">
    <a href="<?php echo get_permalink( $item->ID ); ?>">
        <div class="photo">
            <?php echo wp_get_attachment_image($photo['ID'], 'medium'); ?>
        </div>

        <div class="info">
            <h4><?php echo $term; ?></h4>
            <h3><?php echo $title; ?></h3>

            <div class="copy copy-3">
                <p><?php echo $description; ?></p>
            </div>
        </div>
        
    </a>
</div>