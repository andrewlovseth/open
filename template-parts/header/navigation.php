<nav class="site-navigation">

    <?php get_template_part('template-parts/header/navigation/link-list'); ?>

    <div class="nav-content">
        <?php get_template_part('template-parts/header/navigation/initial-tab'); ?>
    </div>

    <?php get_template_part('template-parts/header/navigation/tab-container'); ?>

</nav>
