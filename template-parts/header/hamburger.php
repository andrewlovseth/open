<div class="hamburger hamburger-mobile">
    <a href="#" class="bun js-mobile-nav-trigger" aria-label="Mobile main menu" title="Mobile main menu">
        <div class="patty"></div>
    </a>
</div>

<div class="hamburger hamburger-desktop">
    <a href="#" class="bun js-nav-trigger" aria-label="Main menu" title="Main menu">
        <div class="patty"></div>
    </a>
</div>